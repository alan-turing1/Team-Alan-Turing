// User Login
const login =  (request, response) => {
    //console.log('login endpoint hit with user: ' + request.body.userEmail + " and " + request.body.userPassword);
    const query = 'SELECT UserID, AccountType FROM users WHERE Email = ? AND Password = ?';
    const params = [request.body.userEmail, request.body.userPassword];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    if(result.length == 0){
                            response.send({ok: true, userFound: false});
                    }
                    else{
                            let query2 = null;
                            switch(result[0].AccountType){
                                    case 1:
                                            query2 = 'SELECT PatientID FROM patient WHERE UserID = ?';
                                            break;
                                    case 2:
                                            query2 = 'SELECT DoctorID FROM doctor WHERE UserID = ?';
                                            break;
                                    case 3:
                                            query2 = 'SELECT CommunityOwnerID FROM communityowner WHERE UserID = ?';
                                            break;
                                    case 4:
                                            query2 = 'SELECT HospitalOwnerID FROM hospitalowner WHERE UserID = ?';
                                            break;
                            }
                            const params2 = [result[0].UserID];
                            const accType = result[0].AccountType;
                            if(query2 !== null){
                                    connection.query(query2, params2, (error, result) => {
                                            if(error !== null){
                                                    response.send({ok: false, errorMessage: error});
                                            }
                                            else{
                                                    var keyID = Object.keys(result[0])[0];
                                                    //console.log(Object.keys(result[0]));
                                                    response.send({ok: true, userFound: true, objectId: result[0][keyID], accountType: accType});
                                            }
                                    });
                            }
                            else{
                                    //console.log("login response sent");
                                    response.send({
                                            ok: true,
                                            userFound: true,
                                            userId: result[0].UserID,
                                            accountType: result[0].AccountType,
                                    });
                            }
                    }
            }
    });
};

// Disapprove a user
const disapproveUser =  (request, response) => {
    const query = 'DELETE FROM users WHERE UserID = ?';
    const params = [request.params.userID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    response.send({
                            ok: true
                    });
            }
    });
};


module.exports = {
    login,
    disapproveUser
}