// const { connection } = require('../app');

//Register new patient
const registerPatient = (request, response) => {
    //console.log("Got patient: " + request.body.fName);
    const query = 'INSERT INTO users(FirstName, MiddleInitial, LastName, Birthdate, StreetAddress, StreetAddress2, City, StateCode, ZipCode, Country, AccountType, Email, Password, SSN, OtherID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, ?, ?, ?, ?)';
    const params = [request.body.fName, request.body.mInitial, request.body.lName, request.body.birthDate, request.body.address1, request.body.address2, request.body.cityName, request.body.stateName, request.body.zipcode, request.body.countryName, request.body.userEmail, request.body.userPassword, request.body.ssnID, request.body.otherGovID];
    let id = null;
    let familyId = null;
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    id = result.insertId;
                    //console.log(request.body.famID);
                    if(request.body.famID === ""){
                            //console.log("inserting");
                            const query2 = 'INSERT INTO family(FamilyInfo) VALUES (?)';
                            const params2 = [''];
                            connection.query(query2, params2, (error, result) => {

                                    familyId = result.insertId;
                                    let hosID = null;
                                    let comID = null;
                                    if(request.body.patientCommunityID !== ""){
                                            comID = request.body.patientCommunityID;
                                    }
                                    if(request.body.patientHospitalID !== ""){
                                            hosID = request.body.patientHospitalID;
                                    }
                                    const query3 = 'INSERT INTO patient(UserID, FamilyID, PrevFamilyID, HospitalID, HospitalApproved, CommunityID, CommunityApproved) VALUES (?, ?, -1, ?, FALSE, ?, FALSE)';
                                    const params3 = [id, familyId, hosID, comID];
                                    connection.query(query3, params3, (error, result) => {
                                            if(error !== null){
                                                    response.send({okay: false, errorMessage: error});
                                            }
                                            else{
                                                    response.send({
                                                            ok: true,
                                                            userId: id,
                                                            famId: familyId,
                                                            patientId: result.insertId,
                                                    });
                                            }
                                    });
                            });
                    }
                    else{
                            familyId = request.body.famID;
                            let hosID = null;
                            let comID = null;
                            if(request.body.patientCommunityID !== ""){
                                    comID = request.body.patientCommunityID;
                            }
                            if(request.body.patientHospitalID !== ""){
                                    hosID = request.body.patientHospitalID;
                            }
                            const query3 = 'INSERT INTO patient(UserID, FamilyID, PrevFamilyID, HospitalID, CommunityID) VALUES (?, ?, ?, ?, ?)';
            let prevFamId = -1;
            if(request.body.prevFamID !== ""){
                prevFamId = request.body.prevFamID;
            }
                            const params3 = [id, familyId, prevFamId, hosID, comID];
                            connection.query(query3, params3, (error, result) => {
                                    if(error !== null){
                                            response.send({
                                                    ok: false,
                                                    errorMessage: error
                                            });
                                    }
                                    else{
                                            response.send({
                                                    ok: true,
                                                    userId: id,
                                                    patientId: result.insertId,
                                            });
                                    }
                            });
                    }
            }
    });
};


module.exports = registerPatient;