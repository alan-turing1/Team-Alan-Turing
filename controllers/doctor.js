// const { connection } = require('../app');

function patientRowsToObject(row){
	return {
		patientID: row.PatientID,
		patientFirstName: row.FirstName,
		patientMiddleInitial: row.MiddleInitial,
		patientLastName: row.LastName,
		patientBirthDate: row.PatientBirthDate,
		patientEmail: row.PatientEmail,
	};
}	

// Register new doctor
const registerDoctor = (request, response) => {
    const query = 'INSERT INTO users(FirstName, MiddleInitial, LastName, Birthdate, StreetAddress, StreetAddress2, City, StateCode, ZipCode, Country, AccountType, Email, Password, SSN, OtherID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 2, ?, ?, ?, ?)';
    const params = [request.body.fName, request.body.mInitial, request.body.lName, request.body.birthDate, request.body.address1, request.body.address2, request.body.cityName, request.body.stateName, request.body.zipcode, request.body.countryName, request.body.userEmail, request.body.userPassword, request.body.ssnID, request.body.otherGovID];
    let id = null;
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    id = result.insertId;
                    let hosID = null;
                    let comID = null;
                    if(request.body.doctorCommunityID !== ""){
                            comID = request.body.doctorCommunityID;
                    }
                    if(request.body.doctorHospitalID !== ""){
                            hosID = request.body.doctorHospitalID;

                    }
                    //console.log(hosID);
                    const query2 = 'INSERT INTO doctor(UserID, CommunityID, CommunityApproved, HospitalID, HospitalApproved, Specialization) VALUES (?, ?, FALSE, ?, FALSE, ?)';
                    const params2 = [id, comID, hosID, request.body.doctorSpecialization];
                    connection.query(query2, params2, (error, result) => {
                            if(error !== null){
                                    response.send({
                                            ok: false,
                                            errorMessage: error
                                    });
                            }
                            else{
                                    //Add doctorID to doctor table????
                                    response.send({
                                            ok: true,
                                            userId: id,
                                            doctorId: result.insertId,
                                    });
                            }
                    });
            }
    });
};

// Get list of patients from hospital or community with doctorID
const getDoctorPatientList = (request, response) => {

    //console.log(request.params.docID);
    let locationID;
    let hosApproved = false;
    const query = "SELECT HospitalApproved FROM doctor where DoctorID = ?";
    const params = [request.params.docID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else if(result.length == 0){
                    response.send({ok: false, errorMessage: "No doctor found with doctorID " + request.params.docID});
            }
            else {
                    let query2;
                    hosApproved = result[0].HospitalApproved;
                    if (hosApproved) {
                            query2 = "SELECT HospitalID FROM doctor where DoctorID = ?";
                    } else {
                            query2 = "SELECT CommunityID FROM doctor where DoctorID = ?";
                    }
                    const params2 = [request.params.docID];
                    connection.query(query2, params2, (error, result) => {
                            if(error !== null){
                                    response.send({
                                            ok: false,
                                            errorMessage: error
                                    });
                            } else {
                                    let query3;
                                    if (hosApproved) {
                                            locationID = result[0].HospitalID;
                                            query3 = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.HospitalID = ? AND patient.HospitalApproved = true';
                                    } else {
                                            locationID = result[0].CommunityID;
                                            query3 = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.CommunityID = ? AND patient.CommunityApproved = true';
                                    }

                                    const params3 = [locationID];
                                    connection.query(query3, params3, (error, result) => {
                                            if(error !== null){
                                                    response.send({
                                                            ok: false,
                                                            errorMessage: error
                                                    });
                                            } else {
                                                    response.send({
                                                            ok: true,
                                                            rows: result.map(patientRowsToObject),
                                                    });
                                            }
                                    });
                            }
                    });
            }
    });
};

// Get list of patients from hospital or community with doctorID and first or last name
const getDoctorPatientListSearch =  (request, response) => {
    //console.log(request.params.docID);
    let locationID;
    let hosApproved = false;
    const query = "SELECT HospitalApproved FROM doctor where DoctorID = ?";
    const params = [request.params.docID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else if(result.length == 0){
                    response.send({ok: false, errorMessage: "No doctor found for doctorID " + request.params.docID});
            }
            else {
                    //console.log(result);
                    let query2;
                    hosApproved = result[0].HospitalApproved;
                    if (hosApproved) {
                            query2 = "SELECT HospitalID FROM doctor where DoctorID = ?";
                    } else {
                            query2 = "SELECT CommunityID FROM doctor where DoctorID = ?";
                    }
                    const params2 = [request.params.docID];
                    connection.query(query2, params2, (error, result) => {
                            if(error !== null){
                                    response.send({
                                            ok: false,
                                            errorMessage: error
                                    });
                            } else {
                                    let query3;
                                    if (hosApproved) {
                                            locationID = result[0].HospitalID;
                                            query3 = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.HospitalID = ? AND patient.HospitalApproved = true AND (users.FirstName LIKE ? OR users.LastName LIKE ?)';
                                    } else {
                                            locationID = result[0].CommunityID;
                                            query3 = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.CommunityID = ? AND patient.CommunityApproved = true AND (users.FirstName LIKE ? OR users.LastName LIKE ?)';
                                    }

                                    const params3 = [locationID, ("%" + request.params.searchTerm + "%"), ("%" + request.params.searchTerm + "%")];
                                    connection.query(query3, params3, (error, result) => {
                                            if(error !== null){
                                                    response.send({
                                                            ok: false,
                                                            errorMessage: error
                                                    });
                                            } else {
                                                    response.send({
                                                            ok: true,
                                                            rows: result.map(patientRowsToObject),
                                                    });
                                            }
                                    });
                            }
                    });
            }
    });
};

module.exports = {
    registerDoctor,
    getDoctorPatientList,
    getDoctorPatientListSearch
}