// const { connection } = require('../app');

function communityRowsToObject(row){
    return {
        communityId: row.CommunityID,
        communityName: row.CommunityName,
        communityAddr: row.CommunityAddress,
        communityAddr2: row.CommunityAddress2,
        communityCity: row.City,
        communityState: row.StateCode,
        communityZip: row.ZipCode,
        communityCountry: row.Country,
    };
}

function patientRowsToObject(row){
	return {
		patientID: row.PatientID,
		patientFirstName: row.FirstName,
		patientMiddleInitial: row.MiddleInitial,
		patientLastName: row.LastName,
		patientBirthDate: row.PatientBirthDate,
		patientEmail: row.PatientEmail,
	};
}	

function unapprovedDoctorToObject(row){
    return {
            userId: row.UserID,
            userEmail: row.Email,
            fName: row.FirstName,
            mInitial: row.MiddleInitial,
            lName: row.LastName,
            doctorId: row.DoctorID
    };
}

function unapprovedPatientToObject(row){
    return {
            userId: row.UserID,
            userEmail: row.Email,
            fName: row.FirstName,
            mInitial: row.MiddleInitial,
            lName: row.LastName,
            patientId: row.PatientID
    };
}

// Get all communities
const getAllCommunities = (request, response) => {
    const query = 'SELECT CommunityID, CommunityName, CommunityAddress, CommunityAddress2, City, StateCode, ZipCode, Country FROM community';
    connection.query(query, (error, result) => {
        response.send({
            ok: true,
            rows: result.map(communityRowsToObject),
        });
    });
};

// Get communities belonging to a community owner
const getComOwnCommunities = (request, response) => {
    const query = 'SELECT * FROM community WHERE CommunityOwnerID = ?';
    const params = [request.params.comOwnerID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    response.send({ok: true, rows: result.map(communityRowsToObject)});
            }
    });
};

// Register a new community owner and community
const registerCommunity = (request, response) => {
    //console.log(request.body);
    const query = 'INSERT INTO users(FirstName, MiddleInitial, LastName, Birthdate, StreetAddress, StreetAddress2, City, StateCode, ZipCode, Country, AccountType, Email, Password, SSN, OtherID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 3, ?, ?, ?, ?)';
    const params = [request.body.fName, request.body.mInitial, request.body.lName, request.body.birthDate, request.body.address1, request.body.address2, request.body.cityName, request.body.stateName, request.body.zipcode, request.body.countryName, request.body.userEmail, request.body.userPassword, request.body.ssnID, request.body.otherGovID];
    let id = null;
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    id = result.insertId;
                    const query2 = 'INSERT INTO communityowner(UserID) VALUES (?)';
                    const params2 = [id];
                    let comOwnerID = null;
                    connection.query(query2, params2, (error, result) => {
                            comOwnerID = result.insertId;
                            const query3 = 'INSERT INTO community(CommunityName, CommunityAddress, CommunityAddress2, City, StateCode, ZipCode, Country, CommunityOwnerID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
                            const params3 = [request.body.comOwnerCommunityName, request.body.communityAddress1, request.body.communityAddress2, request.body.communityCityName, request.body.communityStateName, request.body.communityZipcode, request.body.communityCountryName, comOwnerID];
                            connection.query(query3, params3, (error, result) => {
                                    if(error !== null){
                                            response.send({
                                                    ok: false,
                                                    errorMessage: error,
                                            });
                                    }
                                    else{
                                            response.send({
                                                    ok: true,
                                                    userId: id,
                                                    communityOwnerId: comOwnerID,
                                                    communityId: result.insertId,
                                            });
                                    }
                            });
                    });
            }
    });
};


// Register a new community to an existing community owner
const registerCommunityToOwner =  (request, response) => {
    const query = 'INSERT INTO community(CommunityName, CommunityOwnerID, CommunityAddress, CommunityAddress2, City, StateCode, ZipCode, Country) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
    const params = [request.body.communityName, request.params.comOwnerID, request.body.communityAddress1, request.body.communityAddress2, request.body.communityCityName, request.body.communityStateName, request.body.communityZipcode, request.body.communityCountryName];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    response.send({ok: true, communityID: result.insertId});
            }
    });
};

// Get list of patients from a community with communityID and patient's first or last name
const getComOwnPatientList = (request, response) => {
	const query = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.CommunityID = ? AND patient.CommunityApproved = true AND (users.FirstName LIKE ? OR users.LastName LIKE ?)';
	const params = [request.params.commID, request.params.searchTerm + "%", request.params.searchTerm + "%"];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
				rows: result.map(patientRowsToObject),
			});
		}
	});
};

// Get unapproved community doctors
const getUnapprovedComDoc =  (request, response) => {
    const query = 'SELECT users.UserID, users.Email, users.FirstName, users.MiddleInitial, users.LastName, doctor.DoctorID FROM users INNER JOIN doctor ON doctor.UserID=users.UserID WHERE doctor.CommunityApproved=FALSE AND doctor.communityID = ?';
    const params =[request.params.comID];
    //console.log(request.params.comID);
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    response.send({
                            ok: true,
                            rows: result.map(unapprovedDoctorToObject),
                    });
            }
    });
};

// Approve community doctor
const approveComDoc = (request, response) => {
    const query = 'UPDATE doctor SET CommunityApproved = TRUE WHERE DoctorID = ?';
    const params = [request.params.docID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    response.send({
                            ok: true
                    });
            }
    });
};


// Get unapproved community patients
const getUnapprovedComPat = (request, response) => {
    const query = 'SELECT users.UserID, users.Email, users.FirstName, users.MiddleInitial, users.LastName, patient.PatientID FROM users INNER JOIN patient ON patient.UserID=users.UserID WHERE patient.CommunityApproved=FALSE AND patient.communityID=?';
    const params =[request.params.comID];
    connection.query(query, params, (error, result) => {
            response.send({
                    ok: true,
                    rows: result.map(unapprovedPatientToObject),
            });
    });
};

// Approve community patient
const approveComPat =  (request, response) => {
    const query = 'UPDATE patient SET HospitalApproved = TRUE WHERE PatientID = ?';
    const params = [request.params.patID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    response.send({
                            ok: true
                    });
            }
    });
};

module.exports = {
    getAllCommunities,
    getComOwnCommunities,
    registerCommunity,
    registerCommunityToOwner,
    getComOwnPatientList,
    getUnapprovedComDoc,
    approveComDoc,
    getUnapprovedComPat,
    approveComPat
}