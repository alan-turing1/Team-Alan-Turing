// const { connection } = require('../app');

function patHistRowsToObject(row){
	return {
		visitID: row.VisitID,
		visitDate: row.VisitDate,
		doctorName: row.FirstName + " " + row.MiddleInitial + "" + row.LastName,
		symptomNotes: row.SymptomNotes,
		meetingNotes: row.MeetingNotes
		//patientMeds: row.patientMeds
		//patientLabs: row.patientLabs	
	};
}

// Get a patient's visit history, ordered by visit date descending
const getPatientVisits = (request, response) => {
	const query = 'SELECT PatientDoctorVisit.VisitID, users.FirstName, users.MiddleInitial, users.LastName, PatientDoctorVisit.VisitDate, PatientDoctorVisit.SymptomNotes, PatientDoctorVisit.MeetingNotes FROM PatientDoctorVisit INNER JOIN doctor ON PatientDoctorVisit.DoctorID = doctor.DoctorID INNER JOIN users ON doctor.UserID = users.UserID WHERE PatientDoctorVisit.PatientID = ? ORDER BY VisitDate DESC';
	const params = [request.params.patientID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
				rows: result.map(patHistRowsToObject)
			});
		}
	})
};

// Add a patient visit
const addPatientVisit = (request, response) => {
	const query = 'INSERT INTO PatientDoctorVisit (PatientID, DoctorID, VisitDate, SymptomNotes, MeetingNotes) VALUES (?, ?, ?, ?, ?)';
	const params = [request.params.patientID, request.params.doctorID, request.body.visitDate, request.body.symptomNotes, request.body.meetingNotes, request.params.patientID, request.params.doctorID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
};

// Update Patient Visit ?s: Include Meds and Labs
const updatePatientVisit = (request, response) => {
	const query = 'UPDATE PatientDoctorVisit SET SymptomNotes = ?, MeetingNotes = ? WHERE VisitID = ?';
	const params = [request.body.symptomNotes, request.body.meetingNotes, request.params.visitID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
};

// Delete Patient Visit
const deletePatientVisit = (request, response) => {
	const query = 'DELETE FROM PatientDoctorVisit WHERE VisitID = ?';
	const params = [request.params.visitID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
};

module.exports = {
    getPatientVisits,
    addPatientVisit,
    updatePatientVisit,
    deletePatientVisit
}