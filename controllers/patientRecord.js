// const { connection } = require('../app');

function recordRowsToObject(row){
    return {
            recordID: row.TestDataID,
            testDate: row.TestDate,
            dateSubmitted: row.SubmitDate,
            testName: row.TestName,
            testValue: row.TestData,
            testUnit: row.Unit
    };
}

function recordRowsToObjectAverage(rows){
    let sum = parseFloat(rows[0].TestData);
    let count = 1;
    let date = rows[0].TestDate.toString().substring(0, 16);
    const values = [];
    for(let i = 1; i < rows.length; i++){
            if(date === rows[i].TestDate.toString().substring(0, 16)){
                    sum += parseFloat(rows[i].TestData);

                    count++;
            }
            else{
                    values.push({dateSubmitted: date, testValue: (sum/count), testName: rows[0].TestName, testUnit: rows[0].testUnit});
                    sum = parseFloat(rows[i].TestData);
                    count = 1;
                    date = rows[i].TestDate.toString().substring(0, 16);
            }
    }
    values.push({dateSubmitted: date, testValue: (sum/count), testName: rows[0].TestName, testUnit: rows[0].testUnit});
    return values;
}

// Get all test data of a patient, grouped by test, ordered by date/time
const getAllPatientRecords =  (request, response) => {
	const query = 'SELECT PatientTestData.MedicalTestID, medicaltest.TestName, PatientTestData.TestData, medicaltest.Unit, PatientTestData.TestDate FROM PatientTestData INNER JOIN medicaltest ON PatientTestData.MedicalTestID = medicaltest.MedicalTestID WHERE PatientTestData.PatientID = ? GROUP BY medicaltest.MedicalTestID ORDER BY PatientTestData.TestDate';
	const params = [request.params.patientID];
	connection.query(query, params, (error, result) => {
		response.send({
			ok: true,
			rows: result.map(recordRowsToObject),
		});
	});
};

// Get test data of a patient by test and between two date/time, ordered by date/time
const getDatedPatientRecords = (request, response) => {
    const query = 'SELECT PatientTestData.TestDataID, medicaltest.TestName, PatientTestData.TestData, PatientTestData.TestDate, PatientTestData.SubmitDate, medicaltest.Unit FROM PatientTestData INNER JOIN medicaltest ON PatientTestData.MedicalTestID = medicaltest.MedicalTestID WHERE PatientID = ? AND PatientTestData.MedicalTestID = ? AND PatientTestData.SubmitDate BETWEEN ? AND ? ORDER BY PatientTestData.TestDate';
    const params = [request.params.patientID, request.params.testID, request.params.dateTime1, request.params.dateTime2];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    if(result.length == 0){
                            response.send({ok: true, rows: []});
                    }
                    else{
                            response.send({ok: true, rows: result.map(recordRowsToObject)});
                    }
            }
    });
};


// Get average test data of a patient by test and between two date/time, ordered by date/time
const getDatedAvePatientRecords =  (request, response) => {
    const query = 'SELECT PatientTestData.MedicalTestID, medicaltest.TestName, PatientTestData.TestData, PatientTestData.TestDate, medicaltest.Unit FROM PatientTestData INNER JOIN medicaltest ON PatientTestData.MedicalTestID = medicaltest.MedicalTestID WHERE PatientID = ? AND PatientTestData.MedicalTestID = ? AND PatientTestData.TestDate BETWEEN ? AND ? ORDER BY PatientTestData.TestDate';
    const params = [request.params.patientID, request.params.testID, request.params.dateTime1, request.params.dateTime2];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    if(result.length == 0){
                            response.send({ok: true, rows: []});
                    }
                    else{
                            response.send({ok: true, rows: recordRowsToObjectAverage(result)});
                    }
            }
    });
};

// Add patient test data
const addPatientRecord = (request, response) => {
    const query = 'INSERT INTO PatientTestData(PatientID, MedicalTestID, TestData, TestDate, SubmitDate) VALUES(?, ?, ?, ?, ?)';
    const params = [request.body.patientId, request.body.medicalTestId, request.body.testData, request.body.testDate, request.body.submitDate];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    response.send({ok: true, testId: result.insertId});
            }
    });
};

// Update patient test data value 
const updatePatientRecord =  (request, response) => {
	const query = 'UPDATE PatientTestData SET TestData = ?, TestDate = ? WHERE TestDataID = ?';
	const params = [request.body.testData, request.body.testDate, request.params.TestDataID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					response.send({ok: true});
			}
	});
};

// Delete patient test data
const deletePatientRecord = (request, response) => {
	const query = 'DELETE FROM PatientTestData WHERE TestDataID = ?';
	const params = [request.params.TestDataID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true});
			}
	});
};

module.exports = {
    getAllPatientRecords,
    getDatedPatientRecords,
    getDatedAvePatientRecords,
    addPatientRecord,
    updatePatientRecord,
    deletePatientRecord
}