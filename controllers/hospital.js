// const { connection } = require('../app');

function hospitalRowsToObject(row){
    return {
        hospitalId: row.HospitalID,
        hospitalName: row.HospitalName,
        hospitalAddr: row.HospitalAddress,
        hospitalAddr2: row.HospitalAddress2,
        hospitalCity: row.City,
        hospitalState: row.StateCode,
        hospitalZip: row.ZipCode,
        hospitalCountry: row.Country,
    };
}

function patientRowsToObject(row){
	return {
		patientID: row.PatientID,
		patientFirstName: row.FirstName,
		patientMiddleInitial: row.MiddleInitial,
		patientLastName: row.LastName,
		patientBirthDate: row.PatientBirthDate,
		patientEmail: row.PatientEmail,
	};
}	

function unapprovedDoctorToObject(row){
    return {
            userId: row.UserID,
            userEmail: row.Email,
            fName: row.FirstName,
            mInitial: row.MiddleInitial,
            lName: row.LastName,
            doctorId: row.DoctorID
    };
}

function unapprovedPatientToObject(row){
    return {
            userId: row.UserID,
            userEmail: row.Email,
            fName: row.FirstName,
            mInitial: row.MiddleInitial,
            lName: row.LastName,
            patientId: row.PatientID
    };
}

// Get all hospitals
const getAllHospitals = (request, response) => {
    const query = 'SELECT HospitalID, HospitalName, HospitalAddress, HospitalAddress2, City, StateCode, ZipCode, Country FROM hospital';
    connection.query(query, (error, result) => {
        response.send({
            ok: true,
            rows: result.map(hospitalRowsToObject),
        });
    });
};

// Get hospitals belonging to a hospital owner
const getHosOwnHospitals =  (request, response) => {
    const query = 'SELECT * FROM hospital WHERE HospitalOwnerID = ?';
    const params = [request.params.hosOwnerID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    response.send({ok: true, rows: result.map(hospitalRowsToObject)});
            }
    });
};

// Register a new hospital owner and hospital
const registerHospital = (request, response) => {
    const query = 'INSERT INTO users(FirstName, MiddleInitial, LastName, Birthdate, StreetAddress, StreetAddress2, City, StateCode, ZipCode, Country, AccountType, Email, Password, SSN, OtherID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 4, ?, ?, ?, ?)';
    const params = [request.body.fName, request.body.mInitial, request.body.lName, request.body.birthDate, request.body.address1, request.body.address2, request.body.cityName, request.body.stateName, request.body.zipcode, request.body.countryName, request.body.userEmail, request.body.userPassword, request.body.ssnID, request.body.otherGovID];
    let id = null;
    connection.query(query, params, (error, result) => {

            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    //console.log(error);
                    //console.log(result);
                    id = result.insertId;
                    //console.log(id);
                    const query2 = 'INSERT INTO hospitalowner(UserID) VALUES (?)';
                    const params2 = [id];
                    //console.log(params2);
                    let hosOwnerID = null;
                    connection.query(query2, params2, (error, result) => {
                            //console.log(error);
                            //console.log(result);
                            hosOwnerID = result.insertId;
                            const query3 = 'INSERT INTO hospital(HospitalName, HospitalAddress, HospitalAddress2, City, StateCode, ZipCode, Country, HospitalOwnerID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
                            const params3 = [request.body.hospOwnerHospitalName, request.body.hospitalAddress1, request.body.hospitalAddress2, request.body.hospitalCityName, request.body.hospitalStateName, request.body.hospitalZipcode, request.body.hospitalCountryName, hosOwnerID];
                            connection.query(query3, params3, (error, result) => {
                                    if(error !== null){
                                            response.send({
                                                    ok: false,
                                                    errorMessage: error
                                            });
                                    }
                                    else{
                                    //console.log(error);
                                    //console.log(result);
                                            response.send({
                                                    ok: true,
                                                    userId: id,
                                                    hospitalOwnerId: hosOwnerID,
                                                    hospitalId: result.insertId,
                                            });
                                    }
                            });
                    });
            }
    });
};


// Register hospital to an existing hospital owner
const registerHospitalToOwner = (request, response) => {
    const query = 'INSERT INTO hospital(HospitalName, HospitalOwnerID, HospitalAddress, HospitalAddress2, City, StateCode, ZipCode, Country) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
    const params = [request.body.hospitalName, request.params.hosOwnerID, request.body.hospitalAddress1, request.body.hospitalAddress2, request.body.hospitalCityName, request.body.hospitalStateName, request.body.hospitalZipcode, request.body.hospitalCountryName];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    response.send({ok: true, hospitalID: result.insertId});
            }
    });
};

// Get list of patients from a hospital with hospitalID and patient's first or last name
const getHosOwnPatientList =  (request, response) => {
	const query = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.HospitalID = ? AND patient.HospitalApproved = true AND (users.FirstName LIKE ? OR users.LastName LIKE ?)';
	const params = [request.params.hospID, request.params.searchTerm + "%", request.params.searchTerm + "%"];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
				rows: result.map(patientRowsToObject),
			});
		}
	});
};

// Get unapproved hospital doctors belonging to hospital
const getUnapprovedHosDoc = (request, response) => {
    const query = 'SELECT users.UserID, users.Email, users.FirstName, users.MiddleInitial, users.LastName, doctor.DoctorID FROM users INNER JOIN doctor ON doctor.UserID=users.UserID WHERE doctor.HospitalApproved=FALSE AND doctor.hospitalID=?';
    const params = [request.params.hosID];
    connection.query(query, params, (error, result) => {
            response.send({
                    ok: true,
                    rows: result.map(unapprovedDoctorToObject),
            });
    });
};

// Approve hospital doctor
const approveHosDoc =  (request, response) => {
    const query = 'UPDATE doctor SET HospitalApproved = TRUE WHERE DoctorID = ?';
    const params = [request.params.docID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    response.send({
                            ok: true
                    });
            }
    });
};

// Get unapproved hospital patients belonging to hospital
const getUnapprovedHosPat =  (request, response) => {
    const query = 'SELECT users.UserID, users.Email, users.FirstName, users.MiddleInitial, users.LastName, patient.PatientID FROM users INNER JOIN patient ON patient.UserID=users.UserID WHERE patient.HospitalApproved=FALSE AND patient.hospitalID=?';
    const params = [request.params.hosID];
    connection.query(query, params, (error, result) => {
            response.send({
                    ok: true,
                    rows: result.map(unapprovedPatientToObject),
            });
    });
};

// Approve hospital patient
const approveHosPat =  (request, response) => {
    const query = 'UPDATE patient SET HospitalApproved = TRUE WHERE PatientID = ?';
    const params = [request.params.patID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({
                            ok: false,
                            errorMessage: error
                    });
            }
            else{
                    response.send({
                            ok: true
                    });
            }
    });
};

module.exports = {
    getAllHospitals,
    getHosOwnHospitals,
    registerHospital,
    registerHospitalToOwner,
    getHosOwnPatientList,
    getUnapprovedHosDoc,
    approveHosDoc,
    getUnapprovedHosPat,
    approveHosPat
}