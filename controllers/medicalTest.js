function medicaltestRows(row){
    return {
            testId: row.MedicalTestID,
            testName: row.TestName,
            unitType: row.Unit,
            upperLimit: row.UpperLimit,
            lowerLimit: row.LowerLimit,
            description: row.TestDescription
    };
}

// Get all medical tests
const getMedTests =  (request, response) => {
    const query = 'SELECT * FROM medicaltest';
    connection.query(query, (error, result) => {
            response.send({ok: true, rows: result.map(medicaltestRows)});
    });
};

// Add new medical test as admin
const addMedTest = (request, response) => {
    const query = 'INSERT INTO medicaltest(TestName, Unit, UpperLimit, LowerLimit, TestDescription) VALUES (?, ?, ?, ?, ?)';
    const params = [request.body.testName, request.body.unit, request.body.upperLimit, request.body.lowerLimit, request.body.description];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    response.send({ok: true, testId: result.insertId});
            }
    });
};

// Update medical test with testID
const updateMedTest =  (request, response) => {
    const query = 'UPDATE medicaltest SET TestName = ?, Unit = ?, UpperLimit = ?, LowerLimit = ?, TestDescription = ? WHERE MedicalTestID = ?';
    const params = [request.body.testName, request.body.unit, request.body.upperLimit, request.body.lowerLimit, request.body.description, request.params.testID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    response.send({ok: true});
            }
    });
};

// Delete medical test as admin
const deleteMedTest = (request, response) => {
    const query = 'DELETE FROM medicaltest WHERE MedicalTestID = ?';
    const params = [request.params.testID];
    connection.query(query, params, (error, result) => {
            if(error !== null){
                    response.send({ok: false, errorMessage: error});
            }
            else{
                    response.send({ok: true});
            }
    });
};

module.exports = {
    getMedTests,
    addMedTest,
    updateMedTest,
    deleteMedTest
}