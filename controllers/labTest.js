// const { connection } = require('../app');

function labRowsToObject(row){
	return {
		labID: row.LabID,
		labName: row.LabName,
		labDesc: row.LabDescription,	
	};
}

// Get all lab tests as admin
const getAllLabTests =  (request, response) => {
	const query = "SELECT LabID, LabName, LabDescription FROM LabTest ORDER BY LabName";
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
				rows: result.map(labRowsToObject),
			});
		}
	});
};

// Add lab test as admin
const addLabTest = (request, response,) => {
	const query = 'INSERT INTO LabTest (LabName, LabDescription) VALUES (?, ?)';
	const params = [request.body.labName, request.body.labDesc];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
};

// Update lab test as admin
const updateLabTest = (request, response) => {
	const query = 'UPDATE LabTest SET LabName = ?, LabDescription = ? WHERE LabID = ?';
	const params = [request.body.labName, request.body.labDesc, request.params.labID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
};

// Delete lab test as admin
const deleteLabTest = (request, response) => {
	const query = 'DELETE FROM LabTest WHERE LabID = ?';
	const params = [request.params.labID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
};

module.exports = {
    getAllLabTests,
    addLabTest,
    updateLabTest,
    deleteLabTest
}