// const { connection } = require('../app');

function patLabRowsToObject(row){
	return {
		labID: row.LabID,
		labName: row.LabName,
		labDate: row.LabDate,
		labResult: row.LabResult	
	};
}

// Get all prescribed lab tests of a patient
const getPatientLabTests =  (request, response) => {
	const query = "SELECT PatientLab.LabID, LabTest.LabName, PatientLab.LabDate, PatientLab.LabResult FROM PatientLab JOIN LabTest ON LabTest.LabID = PatientLab.LabID ORDER BY LabTest.LabName";
	const params = [request.params.patientID];
    connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
				rows: result.map(patLabRowsToObject),
			});
		}
	});
};

// Prescribe a lab test to a patient
const prescribeLab = (request, response) => {
	const query = 'INSERT INTO PatientLab (PatientID, LabID, LabDate, LabResults) VALUES (?, ?, ?, ?)';
	const params = [request.params.patientID, request.params.labID, request.params.labDate, "Not Tested Yet"];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
};

// Update lab test results of a patient
const updatePatientLab = (request, response) => {
	const query = 'UPDATE PatientLab SET LabResult = ? WHERE patLabID = ?';
	const params = [request.body.labResult, request.params.patLabID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
};

// Delete lab test of a patient
const deletePatientLab = (request, response) => {
	const query = 'DELETE FROM PatientLab WHERE patLabID = ?';
	const params = [request.params.patLabID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
};

module.exports = {
    getPatientLabTests,
    prescribeLab,
    updatePatientLab,
    deletePatientLab
}