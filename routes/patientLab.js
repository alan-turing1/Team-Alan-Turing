const express = require('express');
const { getPatientLabTests, prescribeLab, updatePatientLab, deletePatientLab } = require('../controllers/patientLab');
const router = express.Router();

// Get all prescribed lab tests of a patient
router.get('/patient_lab_tests/:patientID', getPatientLabTests);

// Prescribe a lab test to a patient
router.post('/prescribe_patient_lab/:patientID/:labID/:labDate', prescribeLab);

// Update lab test results of a patient
router.patch('/update_lab_result/:patLabID', updatePatientLab);

// Delete lab test of a patient
router.delete('/delete_patient_lab/:patLabID', deletePatientLab);

module.export = router;