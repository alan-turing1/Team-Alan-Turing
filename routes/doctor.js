const express = require('express');
const { registerDoctor, getDoctorPatientList, getDoctorPatientListSearch } = require('../controllers/doctor');
const router = express.Router();

// Register new doctor
router.post('/doctor_registration', registerDoctor);

// Get list of patients from hospital or community with doctorID
router.post('/patient_list/doctor/:docID', getDoctorPatientList);

// Get list of patients from hospital or community with doctorID and search term
router.get('/patient_list/doctor/:docID/:searchTerm', getDoctorPatientListSearch);

module.export = router;