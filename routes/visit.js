const express = require('express');
const { getPatientVisits, addPatientVisit, updatePatientVisit, deletePatientVisit } = require('../controllers/visit');
const router = express.Router();

// Get a patient's visit history, ordered by visit date descending
router.get('/patient_visits/:patientID', getPatientVisits);

// Add a patient visit
router.post('/add_visit/:patientID/:doctorID', addPatientVisit);

// Update Patient Visit ?s: Include Meds and Labs
router.patch('/update_visit/:visitID', updatePatientVisit);

// Delete Patient Visit
router.delete('/delete_visit/:visitID', deletePatientVisit);

module.export = router;