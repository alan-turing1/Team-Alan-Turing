const express = require('express');
const { login, disapproveUser } = require('../controllers/user')
const router = express.Router();

// User Login
router.post('/login', login );

// Disapprove user
router.post('/disapprove/:userID', disapproveUser );

module.export = router;