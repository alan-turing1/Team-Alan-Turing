const express = require('express');
const { getAllCommunities, getComOwnCommunities, registerCommunity, registerCommunityToOwner, getComOwnPatientList, getUnapprovedComDoc, approveComDoc, getUnapprovedComPat, approveComPat } = require('../controllers/community');
const router = express.Router();

// Get all communities
router.get('/communities', getAllCommunities);

// Get communities belonging to a community owner
router.get('/getCommunities/:comOwnerID', getComOwnCommunities); 
    
// Register a new community owner and community
router.post('/community_registration', registerCommunity);

// Register a new community to an existing community owner
router.post('/newCommunity/:comOwnerID', registerCommunityToOwner);

// Get list of patients from a community with communityID and patient's first or last name
router.get('/patient_list/community_owner/:commID/:searchTerm', getComOwnPatientList);

// Get unapproved community doctors
router.get('/unapprovedCommunityDoctor/:comID', getUnapprovedComDoc);

// Approve unapproved community doctors
router.patch('/approveCommunityDoctor/:docID', approveComDoc);

// Get unapproved community patients
router.get('/unapprovedCommunityPatient/:comID', getUnapprovedComPat);

// Approve community patient
router.patch('/approveHospitalPatient/:patID', approveComPat);

module.export = router;