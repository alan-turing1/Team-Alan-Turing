const express = require('express');
const { getAllLabTests, addLabTest, updateLabTest, deleteLabTest } = require('../controllers/labTest');
const router = express.Router();

// Get all lab tests as admin ?s: Include Lab Desc
router.get('/lab_tests', getAllLabTests);

// Add lab test as admin ?s: Include Lab Desc
router.post('/add_lab', addLabTest);

// Update lab test as admin ?s: Include Lab Desc
router.patch('/update_lab/:labID', updateLabTest);

// Delete lab test as admin
router.delete('/delete_lab/:labID', deleteLabTest);

module.export = router;