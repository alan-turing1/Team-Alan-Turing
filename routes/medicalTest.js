const express = require('express');
const { getMedTests, addMedTest, updateMedTest, deleteMedTest } = require('../controllers/medicalTest');
const router = express.Router();

// Get all medical tests
router.get('/medicaltests', getMedTests);

// Add medical test as admin
router.post('/createTest', addMedTest);

// Update medical test as admin
router.patch('/editTest/:testID', updateMedTest);

// Delete medical test as admin
router.delete('/deleteTest/:testID', deleteMedTest);

module.export = router;