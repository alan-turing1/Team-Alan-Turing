const express = require('express');
const { registerPatient } = require('../controllers/patient');
const router = express.Router();

//Register new patient
router.post('/patient_registration', registerPatient);

module.export = router;
