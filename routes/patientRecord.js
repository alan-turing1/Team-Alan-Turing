const express = require('express');
const { getAllPatientRecords, getDatedPatientRecords, getDatedAvePatientRecords, addPatientRecord, updatePatientRecord, deletePatientRecord } = require('../controllers/patientRecord');
const router = express.Router();

// Get all test data of a patient, grouped by test, ordered by date/time
router.get('/patient_record/:patientID', getAllPatientRecords);

// Get test data of a patient by test and between two date/time, ordered by date/time
router.get('/patient_record/:patientID/:testID/:dateTime1/:dateTime2', getDatedPatientRecords);

// Get average daily test data of a patient by trest and between two date/time, ordered by date/time
router.get('/patient_record_ave/:patientID/:testID/:dateTime1/:dateTime2', getDatedAvePatientRecords);

// Add patient test data
router.post('/newPatientData', addPatientRecord);

// Update patient test data value 
router.patch('/update_patient_record/:TestDataID', updatePatientRecord);

// Delete patient test data
router.delete('/delete_patient_record/:TestDataID', deletePatientRecord);

module.export = router;