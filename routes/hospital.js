const express = require('express');
const { getAllHospitals,  getHosOwnHospitals, registerHospital, registerHospitalToOwner, getHosOwnPatientList, getUnapprovedHosDoc, approveHosDoc, getUnapprovedHosPat, approveHosPat } = require('../controllers/hospital');
const router = express.Router();

// Get all hospitals
router.get('/hospitals', getAllHospitals);

// Get hospitals belonging to a hospital owner
router.get('/getHospitals/:hosOwnerID', getHosOwnHospitals);

// Register a new hospital owner and hospital
router.post('/hospital_registration', registerHospital);

// Register a new hospital to an exisiting hospital owener
router.post('/newHospital/:hosOwnerID', registerHospitalToOwner);

// Get list of patients from a hospital with hospitalID and patient's first or last name
router.get('/patient_list/hospital_owner/:hospID/:searchTerm', getHosOwnPatientList);

// Get unapproved hospital doctors belonging to hospital
router.get('/unapprovedHospitalDoctor/:hosID', getUnapprovedHosDoc);

// Approve hospital doctor
router.patch('/approveHospitalDoctor/:docID', approveHosDoc);

// Get unapproved hospital patients belonging to hospital
router.get('/unapprovedHospitalPatient/:hosID', getUnapprovedHosPat);

// Approve hospital patient
router.patch('/approveHospitalPatient/:patID', approveHosPat);

module.export = router;

