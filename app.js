const express = require('express');
const cors = require('cors');
const mysql = require('mysql');
const https = require('https');
const fs = require('fs');
const path = require('path/posix');
const router = express.Router();
// const userRoute = require('.routes/user');
// const patientRoute = require('./routes/patient');
// const doctorRoute = require('./routes/doctor');
// const hospitalRoute = require('./routes/hospital');
// const communityRoute = require('./routes/community');
// const medicalTestRoute = require('./routes/medicalTest);
// const labTestRoute = require('./routes/labTest');
// const patientLabRoute = require('/routes/patientLab');
// const visitRoute = require('/routes/visit');

const options = {
	key: fs.readFileSync('/etc/letsencrypt/live/cs485webapi.duckdns.org/privkey.pem'),
	cert: fx.readFileSync('/etc/letsencrypt/live/cs485webapi.duckdns.org/fullchain.pem')
};

const app = express();
app.use(cors());
app.use(express.json());

var httpsServer = https.createServer(options, app);

let connection = mysql.createConnection({
	host: "localhost",
	user: "backend_user",
	password: "SE2Project3565--",
	database: "backenddb"
});
connection.connect();
if(connection.state === 'disconnected'){
	console.log(connection.state)
}

// app.use('/', userRoute)
// app.use('/', patientRoute)
// app.use('/', doctorRoute)
// app.use('/', hospitalRoute)
// app.use('/', medicalTestRoute)
// app.use('/', labTestRoute)
// app.use('/', patientLabRoute)
// app.use('/', visitRoute)

function hospitalRowsToObject(row){
	return {
			hospitalId: row.HospitalID,
			hospitalName: row.HospitalName,
			hospitalAddr: row.HospitalAddress,
			hospitalAddr2: row.HospitalAddress2,
			hospitalCity: row.City,
			hospitalState: row.StateCode,
			hospitalZip: row.ZipCode,
			hospitalCountry: row.Country,
	};
}

function communityRowsToObject(row){
	return {
			communityId: row.CommunityID,
			communityName: row.CommunityName,
			communityAddr: row.CommunityAddress,
			communityAddr2: row.CommunityAddress2,
			communityCity: row.City,
			communityState: row.StateCode,
			communityZip: row.ZipCode,
			communityCountry: row.Country,
	};
}

function unapprovedDoctorToObject(row){
	return {
			userId: row.UserID,
			userEmail: row.Email,
			fName: row.FirstName,
			mInitial: row.MiddleInitial,
			lName: row.LastName,
			doctorId: row.DoctorID
	};
}

function unapprovedPatientToObject(row){
	return {
			userId: row.UserID,
			userEmail: row.Email,
			fName: row.FirstName,
			mInitial: row.MiddleInitial,
			lName: row.LastName,
			patientId: row.PatientID
	};
}

function medicaltestRows(row){
	return {
			testId: row.MedicalTestID,
			testName: row.TestName,
			unitType: row.Unit,
			upperLimit: row.UpperLimit,
			lowerLimit: row.LowerLimit,
			description: row.TestDescription
	};
}

app.post('/login', (request, response) => {
	//console.log('login endpoint hit with user: ' + request.body.userEmail + " and " + request.body.userPassword);
	const query = 'SELECT UserID, AccountType FROM users WHERE Email = ? AND Password = ?';
	const params = [request.body.userEmail, request.body.userPassword];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					if(result.length == 0){
							response.send({ok: true, userFound: false});
					}
					else{
							let query2 = null;
							switch(result[0].AccountType){
									case 1:
											query2 = 'SELECT PatientID FROM patient WHERE UserID = ?';
											break;
									case 2:
											query2 = 'SELECT DoctorID FROM doctor WHERE UserID = ?';
											break;
									case 3:
											query2 = 'SELECT CommunityOwnerID FROM communityowner WHERE UserID = ?';
											break;
									case 4:
											query2 = 'SELECT HospitalOwnerID FROM hospitalowner WHERE UserID = ?';
											break;
							}
							const params2 = [result[0].UserID];
							const accType = result[0].AccountType;
							if(query2 !== null){
									connection.query(query2, params2, (error, result) => {
											if(error !== null){
													response.send({ok: false, errorMessage: error});
											}
											else{
													var keyID = Object.keys(result[0])[0];
													//console.log(Object.keys(result[0]));
													response.send({ok: true, userFound: true, objectId: result[0][keyID], accountType: accType});
											}
									});
							}
							else{
									//console.log("login response sent");
									response.send({
											ok: true,
											userFound: true,
											userId: result[0].UserID,
											accountType: result[0].AccountType,
									});
							}
					}
			}
	});
});

app.post('/createTest', (request, response) => {
	const query = 'INSERT INTO medicaltest(TestName, Unit, UpperLimit, LowerLimit, TestDescription) VALUES (?, ?, ?, ?, ?)';
	const params = [request.body.testName, request.body.unit, request.body.upperLimit, request.body.lowerLimit, request.body.description];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true, testId: result.insertId});
			}
	});
});
app.get('/medicaltests', (request, response) => {
	const query = 'SELECT * FROM medicaltest';
	connection.query(query, (error, result) => {
			response.send({ok: true, rows: result.map(medicaltestRows)});
	});
});
app.patch('/editTest/:testID', (request, response) => {
	const query = 'UPDATE medicaltest SET TestName = ?, Unit = ?, UpperLimit = ?, LowerLimit = ?, TestDescription = ? WHERE MedicalTestID = ?';
	const params = [request.body.testName, request.body.unit, request.body.upperLimit, request.body.lowerLimit, request.body.description, request.params.testID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true});
			}
	});
});

app.delete('/deleteTest/:testID', (request, response) => {
	const query = 'DELETE FROM medicaltest WHERE MedicalTestID = ?';
	const params = [request.params.testID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true});
			}
	});
});

app.post('/newHospital/:hosOwnerID', (request, response) => {
	const query = 'INSERT INTO hospital(HospitalName, HospitalOwnerID, HospitalAddress, HospitalAddress2, City, StateCode, ZipCode, Country) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
	const params = [request.body.hospitalName, request.params.hosOwnerID, request.body.hospitalAddress1, request.body.hospitalAddress2, request.body.hospitalCityName, request.body.hospitalStateName, request.body.hospitalZipcode, request.body.hospitalCountryName];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true, hospitalID: result.insertId});
			}
	});
});

app.post('/newCommunity/:comOwnerID', (request, response) => {
	const query = 'INSERT INTO community(CommunityName, CommunityOwnerID, CommunityAddress, CommunityAddress2, City, StateCode, ZipCode, Country) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
	const params = [request.body.communityName, request.params.comOwnerID, request.body.communityAddress1, request.body.communityAddress2, request.body.communityCityName, request.body.communityStateName, request.body.communityZipcode, request.body.communityCountryName];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true, communityID: result.insertId});
			}
	});
});


app.get('/getCommunities/:comOwnerID', (request, response) => {
	const query = 'SELECT * FROM community WHERE CommunityOwnerID = ?';
	const params = [request.params.comOwnerID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true, rows: result.map(communityRowsToObject)});
			}
	});
});

app.get('/getHospitals/:hosOwnerID', (request, response) => {
	const query = 'SELECT * FROM hospital WHERE HospitalOwnerID = ?';
	const params = [request.params.hosOwnerID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true, rows: result.map(hospitalRowsToObject)});
			}
	});
});


app.post('/newPatientData', (request, response) => {
	const query = 'INSERT INTO PatientTestData(PatientID, MedicalTestID, TestData, TestDate, SubmitDate) VALUES(?, ?, ?, ?, ?)';
	const params = [request.body.patientId, request.body.medicalTestId, request.body.testData, request.body.testDate, request.body.submitDate];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true, testId: result.insertId});
			}
	});
});

// Update patient test data value
app.patch('/update_patient_record/:TestDataID', (request, response) => {
	const query = 'UPDATE PatientTestData SET TestData = ?, TestDate = ? WHERE TestDataID = ?';
	const params = [request.body.testData, request.body.testDate, request.params.TestDataID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					response.send({ok: true});
			}
	});
});

// Delete patient test data
app.delete('/delete_patient_record/:TestDataID', (request, response) => {
	const query = 'DELETE FROM PatientTestData WHERE TestDataID = ?';
	const params = [request.params.TestDataID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					response.send({ok: true});
			}
	});
});

function recordRowsToObject(row){
	return {
			recordID: row.TestDataID,
			testDate: row.TestDate,
			dateSubmitted: row.SubmitDate,
			testName: row.TestName,
			testValue: row.TestData,
			testUnit: row.Unit
	};
}

function recordRowsToObjectAverage(rows){
	let sum = parseFloat(rows[0].TestData);
	let count = 1;
	let date = rows[0].TestDate.toString().substring(0, 16);
	const values = [];
	for(let i = 1; i < rows.length; i++){
			if(date === rows[i].TestDate.toString().substring(0, 16)){
					sum += parseFloat(rows[i].TestData);

					count++;
			}
			else{
					values.push({dateSubmitted: date, testValue: (sum/count), testName: rows[0].TestName, testUnit: rows[0].testUnit});
					sum = parseFloat(rows[i].TestData);
					count = 1;
					date = rows[i].TestDate.toString().substring(0, 16);
			}
	}
	values.push({dateSubmitted: date, testValue: (sum/count), testName: rows[0].TestName, testUnit: rows[0].testUnit});
	return values;
}


// Get test data of a patient by test and between two date/time, ordered by date/time
app.get('/patient_record_ave/:patientID/:testID/:dateTime1/:dateTime2', (request, response) => {
	const query = 'SELECT PatientTestData.MedicalTestID, medicaltest.TestName, PatientTestData.TestData, PatientTestData.TestDate, medicaltest.Unit FROM PatientTestData INNER JOIN medicaltest ON PatientTestData.MedicalTestID = medicaltest.MedicalTestID WHERE PatientID = ? AND PatientTestData.MedicalTestID = ? AND PatientTestData.TestDate BETWEEN ? AND ? ORDER BY PatientTestData.TestDate';
	const params = [request.params.patientID, request.params.testID, request.params.dateTime1, request.params.dateTime2];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					if(result.length == 0){
							response.send({ok: true, rows: []});
					}
					else{
							response.send({ok: true, rows: recordRowsToObjectAverage(result)});
					}
			}
	});
});

app.get('/patient_record/:patientID/:testID/:dateTime1/:dateTime2', (request, response) => {
	const query = 'SELECT PatientTestData.TestDataID, medicaltest.TestName, PatientTestData.TestData, PatientTestData.TestDate, PatientTestData.SubmitDate, medicaltest.Unit FROM PatientTestData INNER JOIN medicaltest ON PatientTestData.MedicalTestID = medicaltest.MedicalTestID WHERE PatientID = ? AND PatientTestData.MedicalTestID = ? AND PatientTestData.SubmitDate BETWEEN ? AND ? ORDER BY PatientTestData.TestDate';
	const params = [request.params.patientID, request.params.testID, request.params.dateTime1, request.params.dateTime2];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({ok: false, errorMessage: error});
			}
			else{
					if(result.length == 0){
							response.send({ok: true, rows: []});
					}
					else{
							response.send({ok: true, rows: result.map(recordRowsToObject)});
					}
			}
	});
});



app.get('/hospitals', (request, response) => {
	const query = 'SELECT HospitalID, HospitalName, HospitalAddress, HospitalAddress2, City, StateCode, ZipCode, Country FROM hospital';
	connection.query(query, (error, result) => {
			response.send({ok: true, rows: result.map(hospitalRowsToObject)});
	});
});

app.get('/communities', (request, response) => {
	const query = 'SELECT CommunityID, CommunityName, CommunityAddress, CommunityAddress2, City, Statecode, ZipCode, Country FROM community';
	connection.query(query, (error, result) => {
			response.send({
					ok: true,
					rows: result.map(communityRowsToObject),
			});
	});
});

app.get('/unapprovedCommunityDoctor/:comID', (request, response) => {
	const query = 'SELECT users.UserID, users.Email, users.FirstName, users.MiddleInitial, users.LastName, doctor.DoctorID FROM users INNER JOIN doctor ON doctor.UserID=users.UserID WHERE doctor.CommunityApproved=FALSE AND doctor.communityID = ?';
	const params =[request.params.comID];
	//console.log(request.params.comID);
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					response.send({
							ok: true,
							rows: result.map(unapprovedDoctorToObject),
					});
			}
	});
});

function patientRowsToObject(row){
	return {
			patientID: row.PatientID,
			patientFirstName: row.FirstName,
			patientMiddleInitial: row.MiddleInitial,
			patientLastName: row.LastName,
			patientBirthDate: row.Birthdate,
			patientEmail: row.Email,
	};
}
app.get('/patient_list/doctor/:docID', (request, response) => {

	//console.log(request.params.docID);
	let locationID;
	let hosApproved = false;
	const query = "SELECT HospitalApproved FROM doctor where DoctorID = ?";
	const params = [request.params.docID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else if(result.length == 0){
					response.send({ok: false, errorMessage: "No doctor found with doctorID " + request.params.docID});
			}
			else {
					let query2;
					hosApproved = result[0].HospitalApproved;
					if (hosApproved) {
							query2 = "SELECT HospitalID FROM doctor where DoctorID = ?";
					} else {
							query2 = "SELECT CommunityID FROM doctor where DoctorID = ?";
					}
					const params2 = [request.params.docID];
					connection.query(query2, params2, (error, result) => {
							if(error !== null){
									response.send({
											ok: false,
											errorMessage: error
									});
							} else {
									let query3;
									if (hosApproved) {
											locationID = result[0].HospitalID;
											query3 = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.HospitalID = ? AND patient.HospitalApproved = true';
									} else {
											locationID = result[0].CommunityID;
											query3 = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.CommunityID = ? AND patient.CommunityApproved = true';
									}

									const params3 = [locationID];
									connection.query(query3, params3, (error, result) => {
											if(error !== null){
													response.send({
															ok: false,
															errorMessage: error
													});
											} else {
													response.send({
															ok: true,
															rows: result.map(patientRowsToObject),
													});
											}
									});
							}
					});
			}
	});
});


app.get('/patient_list/doctor/:docID/:searchTerm', (request, response) => {
	//console.log(request.params.docID);
	let locationID;
	let hosApproved = false;
	const query = "SELECT HospitalApproved FROM doctor where DoctorID = ?";
	const params = [request.params.docID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else if(result.length == 0){
					response.send({ok: false, errorMessage: "No doctor found for doctorID " + request.params.docID});
			}
			else {
					//console.log(result);
					let query2;
					hosApproved = result[0].HospitalApproved;
					if (hosApproved) {
							query2 = "SELECT HospitalID FROM doctor where DoctorID = ?";
					} else {
							query2 = "SELECT CommunityID FROM doctor where DoctorID = ?";
					}
					const params2 = [request.params.docID];
					connection.query(query2, params2, (error, result) => {
							if(error !== null){
									response.send({
											ok: false,
											errorMessage: error
									});
							} else {
									let query3;
									if (hosApproved) {
											locationID = result[0].HospitalID;
											query3 = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.HospitalID = ? AND patient.HospitalApproved = true AND (users.FirstName LIKE ? OR users.LastName LIKE ?)';
									} else {
											locationID = result[0].CommunityID;
											query3 = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.CommunityID = ? AND patient.CommunityApproved = true AND (users.FirstName LIKE ? OR users.LastName LIKE ?)';
									}

									const params3 = [locationID, ("%" + request.params.searchTerm + "%"), ("%" + request.params.searchTerm + "%")];
									connection.query(query3, params3, (error, result) => {
											if(error !== null){
													response.send({
															ok: false,
															errorMessage: error
													});
											} else {
													response.send({
															ok: true,
															rows: result.map(patientRowsToObject),
													});
											}
									});
							}
					});
			}
	});
});



// Get list of patients from a community with communityID and patient's first or last name
app.get('/patient_list/community_owner/:commID/:searchTerm', (request, response) => {
	const query = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.CommunityID = ? AND patient.CommunityApproved = true AND (users.FirstName LIKE ? OR users.LastName LIKE ?)';
	const params = [request.params.commID, request.params.searchTerm + "%", request.params.searchTerm + "%"];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			} else {
					response.send({
							ok: true,
							rows: result.map(patientRowsToObject),
					});
			}
	});
});

// Get list of patients from a hospital with hospitalID and patient's first or last name
app.get('/patient_list/hospital_owner/:hospID/:searchTerm', (request, response) => {
	const query = 'SELECT patient.PatientID, users.FirstName, users.MiddleInitial, users.LastName, users.BirthDate, users.Email FROM patient INNER JOIN users ON patient.UserID=users.UserID WHERE patient.HospitalID = ? AND patient.HospitalApproved = true AND (users.FirstName LIKE ? OR users.LastName LIKE ?)';
	const params = [request.params.hospID, request.params.searchTerm + "%", request.params.searchTerm + "%"];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			} else {
					response.send({
							ok: true,
							rows: result.map(patientRowsToObject),
					});
			}
	});
});

app.get('/unapprovedHospitalDoctor/:hosID', (request, response) => {
	const query = 'SELECT users.UserID, users.Email, users.FirstName, users.MiddleInitial, users.LastName, doctor.DoctorID FROM users INNER JOIN doctor ON doctor.UserID=users.UserID WHERE doctor.HospitalApproved=FALSE AND doctor.hospitalID=?';
	const params = [request.params.hosID];
	connection.query(query, params, (error, result) => {
			response.send({
					ok: true,
					rows: result.map(unapprovedDoctorToObject),
			});
	});
});
app.get('/unapprovedCommunityPatient/:comID', (request, response) => {
	const query = 'SELECT users.UserID, users.Email, users.FirstName, users.MiddleInitial, users.LastName, patient.PatientID FROM users INNER JOIN patient ON patient.UserID=users.UserID WHERE patient.CommunityApproved=FALSE AND patient.communityID=?';
	const params =[request.params.comID];
	connection.query(query, params, (error, result) => {
			response.send({
					ok: true,
					rows: result.map(unapprovedPatientToObject),
			});
	});
});

app.get('/unapprovedHospitalPatient/:hosID', (request, response) => {
	const query = 'SELECT users.UserID, users.Email, users.FirstName, users.MiddleInitial, users.LastName, patient.PatientID FROM users INNER JOIN patient ON patient.UserID=users.UserID WHERE patient.HospitalApproved=FALSE AND patient.hospitalID=?';
	const params = [request.params.hosID];
	connection.query(query, params, (error, result) => {
			response.send({
					ok: true,
					rows: result.map(unapprovedPatientToObject),
			});
	});
});

app.post('/patient_registration', (request, response) => {
	//console.log("Got patient: " + request.body.fName);
	const query = 'INSERT INTO users(FirstName, MiddleInitial, LastName, Birthdate, StreetAddress, StreetAddress2, City, StateCode, ZipCode, Country, AccountType, Email, Password, SSN, OtherID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, ?, ?, ?, ?)';
	const params = [request.body.fName, request.body.mInitial, request.body.lName, request.body.birthDate, request.body.address1, request.body.address2, request.body.cityName, request.body.stateName, request.body.zipcode, request.body.countryName, request.body.userEmail, request.body.userPassword, request.body.ssnID, request.body.otherGovID];
	let id = null;
	let familyId = null;
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					id = result.insertId;
					//console.log(request.body.famID);
					if(request.body.famID === ""){
							//console.log("inserting");
							const query2 = 'INSERT INTO family(FamilyInfo) VALUES (?)';
							const params2 = [''];
							connection.query(query2, params2, (error, result) => {

									familyId = result.insertId;
									let hosID = null;
									let comID = null;
									if(request.body.patientCommunityID !== ""){
											comID = request.body.patientCommunityID;
									}
									if(request.body.patientHospitalID !== ""){
											hosID = request.body.patientHospitalID;
									}
									const query3 = 'INSERT INTO patient(UserID, FamilyID, PrevFamilyID, HospitalID, HospitalApproved, CommunityID, CommunityApproved) VALUES (?, ?, -1, ?, FALSE, ?, FALSE)';
									const params3 = [id, familyId, hosID, comID];
									connection.query(query3, params3, (error, result) => {
											if(error !== null){
													response.send({okay: false, errorMessage: error});
											}
											else{
													response.send({
															ok: true,
															userId: id,
															famId: familyId,
															patientId: result.insertId,
													});
											}
									});
							});
					}
					else{
							familyId = request.body.famID;
							let hosID = null;
							let comID = null;
							if(request.body.patientCommunityID !== ""){
									comID = request.body.patientCommunityID;
							}
							if(request.body.patientHospitalID !== ""){
									hosID = request.body.patientHospitalID;
							}
							const query3 = 'INSERT INTO patient(UserID, FamilyID, PrevFamilyID, HospitalID, CommunityID) VALUES (?, ?, ?, ?, ?)';
			let prevFamId = -1;
			if(request.body.prevFamID !== ""){
				prevFamId = request.body.prevFamID;
			}
							const params3 = [id, familyId, prevFamId, hosID, comID];
							connection.query(query3, params3, (error, result) => {
									if(error !== null){
											response.send({
													ok: false,
													errorMessage: error
											});
									}
									else{
											response.send({
													ok: true,
													userId: id,
													patientId: result.insertId,
											});
									}
							});
					}
			}
	});
});

app.post('/doctor_registration', (request, response) => {
	const query = 'INSERT INTO users(FirstName, MiddleInitial, LastName, Birthdate, StreetAddress, StreetAddress2, City, StateCode, ZipCode, Country, AccountType, Email, Password, SSN, OtherID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 2, ?, ?, ?, ?)';
	const params = [request.body.fName, request.body.mInitial, request.body.lName, request.body.birthDate, request.body.address1, request.body.address2, request.body.cityName, request.body.stateName, request.body.zipcode, request.body.countryName, request.body.userEmail, request.body.userPassword, request.body.ssnID, request.body.otherGovID];
	let id = null;
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					id = result.insertId;
					let hosID = null;
					let comID = null;
					if(request.body.doctorCommunityID !== ""){
							comID = request.body.doctorCommunityID;
					}
					if(request.body.doctorHospitalID !== ""){
							hosID = request.body.doctorHospitalID;

					}
					//console.log(hosID);
					const query2 = 'INSERT INTO doctor(UserID, CommunityID, CommunityApproved, HospitalID, HospitalApproved, Specialization) VALUES (?, ?, FALSE, ?, FALSE, ?)';
					const params2 = [id, comID, hosID, request.body.doctorSpecialization];
					connection.query(query2, params2, (error, result) => {
							if(error !== null){
									response.send({
											ok: false,
											errorMessage: error
									});
							}
							else{
									//Add doctorID to doctor table????
									response.send({
											ok: true,
											userId: id,
											doctorId: result.insertId,
									});
							}
					});
			}
	});
});


app.post('/community_registration', (request, response) => {
	//console.log(request.body);
	const query = 'INSERT INTO users(FirstName, MiddleInitial, LastName, Birthdate, StreetAddress, StreetAddress2, City, StateCode, ZipCode, Country, AccountType, Email, Password, SSN, OtherID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 3, ?, ?, ?, ?)';
	const params = [request.body.fName, request.body.mInitial, request.body.lName, request.body.birthDate, request.body.address1, request.body.address2, request.body.cityName, request.body.stateName, request.body.zipcode, request.body.countryName, request.body.userEmail, request.body.userPassword, request.body.ssnID, request.body.otherGovID];
	let id = null;
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					id = result.insertId;
					const query2 = 'INSERT INTO communityowner(UserID) VALUES (?)';
					const params2 = [id];
					let comOwnerID = null;
					connection.query(query2, params2, (error, result) => {
							comOwnerID = result.insertId;
							const query3 = 'INSERT INTO community(CommunityName, CommunityAddress, CommunityAddress2, City, StateCode, ZipCode, Country, CommunityOwnerID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
							const params3 = [request.body.comOwnerCommunityName, request.body.communityAddress1, request.body.communityAddress2, request.body.communityCityName, request.body.communityStateName, request.body.communityZipcode, request.body.communityCountryName, comOwnerID];
							connection.query(query3, params3, (error, result) => {
									if(error !== null){
											response.send({
													ok: false,
													errorMessage: error,
											});
									}
									else{
											response.send({
													ok: true,
													userId: id,
													communityOwnerId: comOwnerID,
													communityId: result.insertId,
											});
									}
							});
					});
			}
	});
});

app.post('/hospital_registration', (request, response) => {
	const query = 'INSERT INTO users(FirstName, MiddleInitial, LastName, Birthdate, StreetAddress, StreetAddress2, City, StateCode, ZipCode, Country, AccountType, Email, Password, SSN, OtherID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 4, ?, ?, ?, ?)';
	const params = [request.body.fName, request.body.mInitial, request.body.lName, request.body.birthDate, request.body.address1, request.body.address2, request.body.cityName, request.body.stateName, request.body.zipcode, request.body.countryName, request.body.userEmail, request.body.userPassword, request.body.ssnID, request.body.otherGovID];
	let id = null;
	connection.query(query, params, (error, result) => {

			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					//console.log(error);
					//console.log(result);
					id = result.insertId;
					//console.log(id);
					const query2 = 'INSERT INTO hospitalowner(UserID) VALUES (?)';
					const params2 = [id];
					//console.log(params2);
					let hosOwnerID = null;
					connection.query(query2, params2, (error, result) => {
							//console.log(error);
							//console.log(result);
							hosOwnerID = result.insertId;
							const query3 = 'INSERT INTO hospital(HospitalName, HospitalAddress, HospitalAddress2, City, StateCode, ZipCode, Country, HospitalOwnerID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
							const params3 = [request.body.hospOwnerHospitalName, request.body.hospitalAddress1, request.body.hospitalAddress2, request.body.hospitalCityName, request.body.hospitalStateName, request.body.hospitalZipcode, request.body.hospitalCountryName, hosOwnerID];
							connection.query(query3, params3, (error, result) => {
									if(error !== null){
											response.send({
													ok: false,
													errorMessage: error
											});
									}
									else{
									//console.log(error);
									//console.log(result);
											response.send({
													ok: true,
													userId: id,
													hospitalOwnerId: hosOwnerID,
													hospitalId: result.insertId,
											});
									}
							});
					});
			}
	});
});

app.patch('/approveCommunityDoctor/:docID', (request, response) => {
	const query = 'UPDATE doctor SET CommunityApproved = TRUE WHERE DoctorID = ?';
	const params = [request.params.docID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					response.send({
							ok: true
					});
			}
	});
});

app.patch('/approveHospitalDoctor/:docID', (request, response) => {
	const query = 'UPDATE doctor SET HospitalApproved = TRUE WHERE DoctorID = ?';
	const params = [request.params.docID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					response.send({
							ok: true
					});
			}
	});
});

app.patch('/approveCommunityPatient/:patID', (request, response) => {
	const query = 'UPDATE patient SET CommunityApproved = TRUE WHERE PatientID = ?';
	const params = [request.params.patID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					response.send({
							ok: true
					});
			}
	});
});

app.patch('/approveHospitalPatient/:patID', (request, response) => {
	const query = 'UPDATE patient SET HospitalApproved = TRUE WHERE PatientID = ?';
	const params = [request.params.patID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					response.send({
							ok: true
					});
			}
	});
});

app.delete('/disapprove/:userID', (request, response) => {
	const query = 'DELETE FROM users WHERE UserID = ?';
	const params = [request.params.userID];
	connection.query(query, params, (error, result) => {
			if(error !== null){
					response.send({
							ok: false,
							errorMessage: error
					});
			}
			else{
					response.send({
							ok: true
					});
			}
	});
});

function labRowsToObject(row){
	return {
		labID: row.LabID,
		labName: row.LabName,
		labDesc: row.LabDescription,	
	};
}

// Get all lab tests as admin
app.get('/lab_tests', (request, response) => {
	const query = "SELECT LabID, LabName, LabDescription FROM LabTest ORDER BY LabName";
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
				rows: result.map(labRowsToObject),
			});
		}
	});
});

// Get lab test with searchTerm (LabName) as doctor
app.get('/lab_test/:searchTerm', (request, response) => {
	const query = "SELECT LabID, LabName, LabDescription FROM LabTest WHERE LabName = ?";
	const params = [request.params.searchTerm + "%"]
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
				rows: result.map(labRowsToObject),
			});
		}
	});
});

// Add lab test as admin 
app.post('/add_lab', (request, response) => {
	const query = 'INSERT INTO LabTest (LabName, LabDescription) VALUES (?, ?)';
	const params = [request.body.labName, request.body.labDesc];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
});

// Update lab test as admin
app.patch('/update_lab/:labID', (request, response) => {
	const query = 'UPDATE LabTest SET LabName = ?, LabDescription = ? WHERE LabID = ?';
	const params = [request.body.labName, request.body.labDesc, request.params.labID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
});

// Delete lab test as admin
app.delete('/delete_lab/:labID', (request, response) => {
	const query = 'DELETE FROM LabTest WHERE LabID = ?';
	const params = [request.params.labID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
});


function patLabsRowsToObject(row){
	return {
		labID: row.LabID,
		labName: row.LabName,
	};
}

// Get all prescribed lab tests of a patient
app.get('/patient_lab_tests/:visitID', (request, response) => {
	const query = "SELECT LabLinking.LabID, LabTest.LabName FROM LabLinking INNER JOIN LabTest ON LabLinking.LabID = LabTest.LabID WHERE LabLinking.VisitID = ? ORDER BY LabTest.LabName";
	const params = [request.params.visitID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else {
			response.send({
				ok: true,
				rows: result.map(patLabsRowsToObject)
			});
		}
	});
});

// Prescribe a lab test to a patient
app.post('/prescribe_patient_lab/:visitID/:labID', (request, response) => {
	const query = 'INSERT INTO LabLinking (VisitID, LabID) VALUES (?, ?)';
	const params = [request.params.visitID, request.params.labID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
});

// Update lab test of a patient
app.patch('/update_patient_lab/:visitID', (request, response) => {
	const query = 'UPDATE LabLinking SET LabID = ? WHERE VisitID = ?';
	const params = [request.body.labID, request.params.visitID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
});

// Delete lab test of a patient
app.delete('/delete_patient_lab/:visitID/:labID', (request, response) => {
	const query = 'DELETE FROM LabLinking WHERE VisitID = ? AND LabID = ?';
	const params = [request.params.visitID, request.params.labID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
});

function patHistRowsToObject(row){
	return {
		visitID: row.VisitID,
		visitDate: row.VisitDate,
		doctorName: row.FirstName + " " + row.MiddleInitial + " " + row.LastName,
		symptomNotes: row.SymptomNotes,
		meetingNotes: row.MeetingNotes,
		patientMeds: null,
		patientLabs: null
	};
}

function patMedsRowsToObject(row){
	return {
		drugID: row.DrugID,
		drugName: row.DrugName,
		drugDose: row.Dosage,
		drugUnits: row.Units
	}
}

// Get a patient's visit history, ordered by visit date descending
app.get('/patient_visits/:patientID', (request, response) => {
	let visits;
	const query = 'SELECT PatientDoctorVisit.VisitID, users.FirstName, users.MiddleInitial, users.LastName, PatientDoctorVisit.VisitDate, PatientDoctorVisit.SymptomNotes, PatientDoctorVisit.MeetingNotes FROM PatientDoctorVisit INNER JOIN doctor ON PatienttDoctorVisit.DoctorID = doctor.DoctorID INNER JOIN users ON doctor.UserID = users.UserID WHERE PatientDoctorVisit.PatientID = ? ORDER BY PatientDoctorVisit.VisitDate DESC';
	const params = [request.params.patientID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			let medsQuery;
			let medsParams;
			let labsQuery;
			let labsParams;
			visits = result.map(patHistRowsToObject);
			for ( let i = 0; i < result.length; i++ ) {
				medsQuery = 'SELECT DrugLinking.DrugID, PrescriptionDrug.DrugName,  PrescriptionDrug.Dosage, PrescriptionDrug.Units FROM DrugLinking INNER JOIN PrescriptionDrug ON DrugLinking.DrugID = PrescriptionDrug.DrugID WHERE DrugLinking.VisitID = ? ORDER BY PrescriptionDrug.DrugName'
				medsParams = [result[i].VisitID];
				connection.query(medsQuery, medsParams, (error, medsResult) => {
					if(error !== null){
						response.send({
							ok: false,
							errorMessage: error
						});
					} else {
						visits[i].patientMeds = medsResult.map(patMedsRowsToObject);
					}
				});
				labsQuery = 'SELECT LabLinking.LabID, LabTest.LabName FROM LabLinking INNER JOIN LabTest ON LabLinking.LabID = LabTest.LabID WHERE DrugLinking.VisitID = ? ORDER BY LabDrug.LabName'
				labsParams = [result[i].VisitID];
				connection.query(labsQuery, labsParams, (error, labsResult) => {
					if(error !== null){
						response.send({
							ok: false,
							errorMessage: error
						});
					} else {
						visits[i].patientLabs = labsResult.map(patLabsRowsToObject);
					}
				});
			}
			response.send({
				ok: true,
				visits: visits
			});
		}
	})
});

function docListRowsToObject(row){
	return {
		doctorID: row.DoctorID,
		doctorName: row.FirstName + " " + row.MiddleInitial + " " + row.LastName,
		comId: row.CommunityID,
		hospId: row.HospitalID
	};
}

//List doctors in patient's community or hospital
app.get('/doctor_list/:patientID', (request, response) => {
	const query = 'SELECT doctor.DoctorID, users.FirstName, users.MiddleInitial users.LastName, doctor.CommunityID, doctor.HospitalID FROM doctor INNER JOIN users ON doctor.UserID = users.UserID WHERE CommunityID = (SELECT CommunityID FROM patient WHERE patientID = ?) OR HospitalID = (SELECT HospitalID FROM patient WHERE patientID = ?)';
	const params = [request.params.patientID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
				rows: result.map(docListRowsToObject)
			});
		}
	})
});

// Add a patient visit
app.post('/add_visit/:patientID/:doctorID', (request, response) => {
	const query = 'INSERT INTO PatientDoctorVisit (PatientID, DoctorID, VisitDate, SymptomNotes, MeetingNotes) VALUES (?, ?, ?, ?, ?)';
	const params = [request.params.patientID, request.params.doctorID, request.body.visitDate, request.body.symptomNotes, request.body.meetingNotes];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
});

// Update Patient Visit ?s: Include Meds and Labs
app.patch('/update_visit/:visitID', (request, response) => {
	const query = 'UPDATE PatientDoctorVisit SET SymptomNotes = ?, MeetingNotes = ? WHERE VisitID = ?';
	const params = [request.body.symptomNotes, request.body.meetingNotes, request.params.visitID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			response.send({
				ok: true,
			});
		}
	})
});

// Delete Patient Visit
app.delete('/delete_visit/:visitID', (request, response) => {
	const query = 'DELETE FROM LabLinking WHERE VisitID = ?';
	const params = [request.params.visitID];
	connection.query(query, params, (error, result) => {
		if(error !== null){
			response.send({
				ok: false,
				errorMessage: error
			});
		} else { 
			const query2 = 'DELETE FROM DrugLinking WHERE VisitID = ?';
			const params2 = [request.params.visitID];
			connection.query(query2, params2, (error, result) => {
				if(error !== null){
					response.send({
						ok: false,
						errorMessage: error
					});
				} else { 
					const query3 = 'DELETE FROM PatientDoctorVisit WHERE VisitID = ?';
					const params3 = [request.params.visitID];
					connection.query(query3, params3, (error, result) => {
						if(error !== null){
							response.send({
								ok: false,
								errorMessage: error
							});
						} else { 
							response.send({
								ok: true,
							});
						}
					});
				}
			});
		}
	})
});

const port = 3443;
httpsServer.listen(port, () => {
	console.log(`We're live on port ${port}`);
});

// module.export = connection;

